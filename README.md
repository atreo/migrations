Atreo/Migrations
===========================


Installation
------------

```sh
$ composer require atreo/migrations
```

Extension configuration:

```
migrations: Atreo\Migrations\Di\MigrationsExtension

migrations:
	table: => DoctrineMigrationVersions
	mainDirectory: => %appDir%/Migrations
	otherDirectories:
	    - one
	    - two 
	    - three
	namespace: DoctrineMigrations
```

Run migrations:

```sh
$ php www/index.php migrations:migrate
```