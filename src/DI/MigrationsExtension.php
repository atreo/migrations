<?php

namespace Atreo\Migrations\DI;

use Atreo\Migrations\Configuration;
use Atreo\Migrations\FixturesRunner;
use Atreo\Migrations\MigrationRunner;
use Doctrine\DBAL\Migrations\Tools\Console\Command\AbstractCommand;
use Nette\DI\CompilerExtension;
use Symfony\Component\Console\Application;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class MigrationsExtension extends CompilerExtension
{

	/**
	 * @var string[]
	 */
	private $defaults = [
		'table' => 'DoctrineMigrationVersions',
		'mainDirectory' => '%appDir%/Migrations',
		'otherDirectories' => [],
		'namespace' => 'DoctrineMigrations',
	];



	/**
	 * {@inheritdoc}
	 */
	public function loadConfiguration()
	{
		$containerBuilder = $this->getContainerBuilder();

		$this->compiler->parseServices(
			$containerBuilder,
			$this->loadFromFile(__DIR__ . '/../Config/services.neon')
		);

		$config = $this->getConfig($this->defaults);
		$this->addConfigurationDefinition($config);
	}



	/**
	 * {@inheritdoc}
	 */
	public function beforeCompile()
	{
		$containerBuilder = $this->getContainerBuilder();
		$containerBuilder->prepareClassList();

		$this->setConfigurationToCommands();
		$this->loadCommandsToApplication();
	}



	private function addConfigurationDefinition(array $config)
	{
		$containerBuilder = $this->getContainerBuilder();

		$containerBuilder->addDefinition($this->prefix('migrationRunner'))
			->setClass(MigrationRunner::class);

		$containerBuilder->addDefinition($this->prefix('fixturesRunner'))
			->setClass(FixturesRunner::class);

		$configurationDefinition = $containerBuilder->addDefinition($this->prefix('configuration'));
		$configurationDefinition->setClass(Configuration::class)
			->addSetup('setMigrationsTableName', [$config['table']])
			->addSetup('setMigrationsDirectory', [$config['mainDirectory']])
			->addSetup('setMigrationsNamespace', [$config['namespace']]);

		$configurationDefinition->addSetup('addMigrationDir', [$config['mainDirectory']]);

		foreach ($config['otherDirectories'] as $migrationDir) {
			$configurationDefinition->addSetup('addMigrationDir', [$migrationDir]);
		}

		$configurationDefinition->addSetup('register');
	}



	private function setConfigurationToCommands()
	{
		$containerBuilder = $this->getContainerBuilder();
		$configurationDefinition = $containerBuilder->getDefinition($containerBuilder->getByType(Configuration::class));

		foreach ($containerBuilder->findByType(AbstractCommand::class) as $commandDefinition) {
			$commandDefinition->addSetup('setMigrationConfiguration', ['@' . $configurationDefinition->getClass()]);
		}
	}



	private function loadCommandsToApplication()
	{
		$containerBuilder = $this->getContainerBuilder();
		$applicationDefinition = $containerBuilder->getDefinition($containerBuilder->getByType(Application::class));
		foreach ($containerBuilder->findByType(AbstractCommand::class) as $name => $commandDefinition) {
			$applicationDefinition->addSetup('add', ['@' . $name]);
		}
	}

}
