<?php

namespace Atreo\Migrations;

use Doctrine\DBAL\Migrations\Version;
use Doctrine\ORM\EntityManager;
use Nette\DI\Container;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
abstract class AbstractMigration extends \Doctrine\DBAL\Migrations\AbstractMigration
{

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var Container
	 */
	protected $container;



	public function __construct(Version $version)
	{
		parent::__construct($version);
		$this->container = $version->getConfiguration()->getContainer();
		$this->em = $this->container->getByType(EntityManager::class);
	}

}
