<?php

namespace Atreo\Migrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\OutputWriter;
use Nette\DI\Container;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Configuration extends \Doctrine\DBAL\Migrations\Configuration\Configuration
{

	/**
	 * @var \Nette\DI\Container
	 */
	private $container;

	/**
	 * @var array
	 */
	private $dirs = [];



	/**
	 * @param \Nette\DI\Container $container
	 * @param \Doctrine\DBAL\Connection $connection
	 * @param \Doctrine\DBAL\Migrations\OutputWriter|NULL $outputWriter
	 */
	public function __construct(Container $container, Connection $connection, OutputWriter $outputWriter = NULL)
	{
		$this->container = $container;
		parent::__construct($connection, $outputWriter);
	}



	/**
	 * @return Container
	 */
	public function getContainer()
	{
		return $this->container;
	}



	/**
	 * {@inheritdoc}
	 */
	public function getMigrationsToExecute($direction, $to)
	{
		$versions = parent::getMigrationsToExecute($direction, $to);
		foreach ($versions as $version) {
			$this->container->callInjects($version->getMigration());
		}
		return $versions;
	}



	/**
	 * {@inheritdoc}
	 */
	public function getVersion($version)
	{
		$version = parent::getVersion($version);
		$this->container->callInjects($version->getMigration());
		return $version;
	}



	/**
	 * {@inheritdoc}
	 */
	public function setMigrationsDirectory($directory)
	{
		$this->createDirectoryIfNotExists($directory);
		parent::setMigrationsDirectory($directory);
	}



	/**
	 * @param string $dir
	 */
	public function addMigrationDir($dir)
	{
		$this->dirs[$dir] = $dir;
	}



	public function register()
	{
		foreach ($this->dirs as $dir) {
			$this->registerMigrationsFromDirectory($dir);
		}
	}



	/**
	 * {@inheritdoc}
	 */
	public function registerMigration($version, $class)
	{
		$this->ensureMigrationClassExists($class);
		parent::registerMigration($version, $class);
	}



	/**
	 * @param string $directory
	 */
	private function createDirectoryIfNotExists($directory)
	{
		if (!file_exists($directory)) {
			mkdir($directory, 0755, TRUE);
		}
	}



	/**
	 * @param string $class
	 * @throws MigrationClassNotFoundException
	 */
	private function ensureMigrationClassExists($class)
	{
		if (!class_exists($class)) {
			throw new MigrationClassNotFoundException(
				sprintf(
					'Migration class "%s" was not found. Is it placed in "%s" namespace?',
					$class,
					$this->getMigrationsNamespace()
				)
			);
		}
	}

}



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class MigrationClassNotFoundException extends \Exception {}
