<?php

namespace Atreo\Migrations;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\ORM\EntityManager;
use Nette\DI\Container;
use Nette\Object;
use Nette\Utils\Finder;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FixturesRunner extends Object
{

	/**
	 * @var \Nette\DI\Container
	 */
	private $container;

	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $em;



	/**
	 * @param \Nette\DI\Container $container
	 * @param \Doctrine\ORM\EntityManager $em
	 */
	public function __construct(Container $container, EntityManager $em)
	{
		$this->container = $container;
		$this->em = $em;
	}



	public function run()
	{
		$appDir = $this->container->getParameters()['appDir'];

		$loader = new Loader();

		if (file_exists($appDir . '/Fixtures')) {
			$loader->loadFromDirectory($appDir . '/Fixtures');
		}

		/** @var \SplFileInfo[] $modules */
		$modules = Finder::findDirectories('*')
			->in($appDir . '/../vendor/atreo/');

		foreach ($modules as $module) {
			if (!Strings::match($module->getPathname(), '/admin/')) { continue; }
			$fixturesDir = $module->getPathname() . '/src/Atreo/Fixtures';
			if (file_exists($fixturesDir)) {
				$loader->loadFromDirectory($module->getPathname() . '/src/Atreo/Fixtures');
			}
		}

		$fixtures = [];
		foreach ($loader->getFixtures() as $fixture) {
			$fixtures[] = $this->container->createInstance(get_class($fixture));
		}

		$executor = new ORMExecutor($this->em);
		$executor->execute($fixtures, TRUE);
	}

}
