<?php

namespace Atreo\Migrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\Migration;
use Nette\DI\Container;
use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class MigrationRunner extends Object
{

	/**
	 * @var \Nette\DI\Container
	 */
	private $container;

	/**
	 * @var Configuration
	 */
	private $configuration;



	/**
	 * @param \Nette\DI\Container $container
	 */
	public function __construct(Container $container)
	{
		$this->container = $container;
		$this->configuration = $this->container->getByType(Configuration::class);
	}



	/**
	 * @throws \Doctrine\DBAL\Migrations\MigrationException
	 */
	public function run()
	{
		/** @var Connection $connection */
		$connection = $this->container->getByType(Connection::class);

		$this->configuration->__construct($this->container, $connection);

		$migration = new Migration($this->configuration);
		$migration->migrate();
	}

}
